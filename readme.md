# RN-Contacts-App

Application to get the contacts from your mobile device

## Installation

```bash
npm install
```

## Start

```python
expo run:android
```
